
class DateCalculator::Date
  extend DateCalculator::Parser
  include Comparable
  include DateCalculator::Validator
  include DateCalculator::Leap
  include DateCalculator::JulianDate

  attr_accessor :year, :month, :day

  def initialize(year, month, day)
    @year = year
    @month = month
    @day = day

    raise DateCalculator::InvalidInput, "Invalid Date" unless valid?
  end

  def to_i
    to_julian.to_i
  end

  def -(other)
    to_julian - other.to_julian
  end

  def <=>(other)
    to_julian <=> other.to_julian
  end
end
