require 'date'

class DateCalculator::Calculator
  include DateCalculator::Validator

  attr_accessor :start_date, :finish_date

  def initialize(start_date, finish_date)
    @start_date = start_date
    @finish_date = finish_date
    
    correct_order
  end

  def calculate
    [(finish_date - start_date).to_i - adjustment, 0].max
  end

  private

  # adjustment date to take into account
  # partial experiment days
  def adjustment
    1
  end

  def correct_order
    if @start_date > @finish_date
      @finish_date, @start_date = @start_date, @finish_date
    end
  end
end
