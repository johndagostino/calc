
# Mixin for validating input dates are within
# the defined experiments ranges
module DateCalculator::Validator
  DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

  def valid?
    year.between?(1901, 2999) && \
    month.between?(1, 12) && \
    day.between?(1, days_in_month(month, year))
  end

  private

  def days_in_month(month, year)
    day = DAYS[month - 1]

    if (leap? && month == 2)
      day + 1
    else
      day
    end
  end
end
