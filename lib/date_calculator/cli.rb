
module DateCalculator
  module Cli
    def self.run
      start, finish = nil

      ARGV << '-h' if ARGV.empty?

      OptionParser.new do |opts|
        opts.banner = "Usage: date_calculator.rb [options]"
        opts.on("-s", "--start [date]", "Start date") do |date|
          start = date
        end
        opts.on("-f", "--finish [date]", "Finish date") do |date|
          finish = date
        end

        opts.on_tail("-h", "--help", "Show this message") do
          puts opts
          exit
        end
      end.parse!

      start_date = DateCalculator::Date.parse(start)
      finish_date = DateCalculator::Date.parse(finish)

      puts DateCalculator::Calculator.new(start_date,
                                          finish_date).calculate
    end
  end
end
