
module DateCalculator::Parser
  def parse(input)
    day, month, year = input.split /\//

    if year.nil? || month.nil? || day.nil?
      raise ArgumentError, "Invalid Date"
    end

    new(year.to_i, month.to_i, day.to_i)
  end
end
