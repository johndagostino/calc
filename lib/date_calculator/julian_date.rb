module DateCalculator::JulianDate
  def to_julian
    a = (14 - month)/12
    y = year + 4800 - a
    m = month + 12 * a - 3
    jdn = day + (153 * m + 2)/5 + 365 * y + y/4 - y/100 + y/400 - 32045 - 0.5
    jdn + (0-12)/24 + 0/1440 + 0/86400 # midnight
  end
end
