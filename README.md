# Date Calculator

You have joined a science project as the latest team member. Scientists on the project are running a series
of experiments and need to calculate the number of full days elapsed in between the experiment’s start and
end dates, i.e. the first and the last day are considered partial days and never counted.

Following this logic, an experiment that has run from 07/11/1972 and 08/11/1972 should return 0, because there are no
fully elapsed days contained in between those dates, and 01/01/2000 to 03/01/2000 should return 1. The solution
needs to cater for all valid dates between 01/01/1901 and 31/12/2999.

## Installation

Tested with Ruby 2.2

Currently this isn't published to RubyGems, so you have to install from git

Once published you would add this line to your application's Gemfile:

```ruby
gem 'date_calculator'
```

And then execute:

    $ bundle

Or install it globally yourself as:

    $ gem install date_calculator

## Usage

    $ bundle exec date_calculator -s start_date -f finish_date

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/date_calculator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

