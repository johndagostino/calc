require 'spec_helper'

describe DateCalculator::Date do
  context 'default' do
    subject { DateCalculator::Date.new(2000, 12, 10) }
    it { expect(subject.year).to eql(2000) }
    it { expect(subject.month).to eql(12) }
    it { expect(subject.day).to eql(10) }
  end

  context 'spaceship operator' do
    subject { DateCalculator::Date.new(2000, 12, 10) }
    let(:other) { DateCalculator::Date.new(2010, 12, 1) }

    it { expect(subject > other).to eql(false) }
    it { expect(subject < other).to eql(true) }
  end

  context 'difference' do
    let(:first) { DateCalculator::Date.parse('03/08/1983') }
    let(:second) { Date.parse('03/08/1983') }

    it { expect(first.year).to eql(second.year) }
    it { expect(first.month).to eql(second.month) }
    it { expect(first.day).to eql(second.day) }
  end
end
