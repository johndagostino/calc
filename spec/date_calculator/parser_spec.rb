require 'spec_helper'

class DateExample
  attr_accessor :year, :month, :day

  def initialize(year, month, day)
    @year = year
    @month = month
    @day = day
  end

  extend DateCalculator::Parser
end

describe DateCalculator::Parser do
  context 'correct format' do
    subject { DateExample.parse('10/12/2000') }
    it { expect(subject.year).to eql(2000) }
    it { expect(subject.month).to eql(12) }
    it { expect(subject.day).to eql(10) }
  end

  it 'raises an exception if invalid' do
    expect do
      DateExample.parse('10/12')
    end.to raise_error(ArgumentError)
  end
end
