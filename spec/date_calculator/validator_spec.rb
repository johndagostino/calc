require 'spec_helper'

class MyDate
  include DateCalculator::Validator
  include DateCalculator::Leap

  attr_accessor :year, :month, :day

  def initialize(year, month, day)
    @year = year
    @month = month
    @day = day
  end

end

describe DateCalculator::Validator do
  context 'boundary years' do
    it { expect(MyDate.new(1000, 12, 10).valid?).to eql(false) }
    it { expect(MyDate.new(3001, 12, 3).valid?).to eql(false) }
    it { expect(MyDate.new(2015, 12, 10).valid?).to eql(true) }
    it { expect(MyDate.new(2016, 2, 29).valid?).to eql(true) }
    it { expect(MyDate.new(2016, 14, 29).valid?).to eql(false) }
    it { expect(MyDate.new(2015, 13, 29).valid?).to eql(false) }
    it { expect(MyDate.new(2017, 2, 29).valid?).to eql(false) }
  end
end
