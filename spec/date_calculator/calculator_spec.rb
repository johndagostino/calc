require 'spec_helper'

describe DateCalculator::Calculator do
  let (:calculator) { DateCalculator::Calculator.new(start_date, end_date) }
  let(:date_class) { DateCalculator::Date }

  context 'same month' do
    let(:start_date) { date_class.parse('02/06/1983') }
    let(:end_date) { date_class.parse('22/06/1983') }
    let(:result) { 19 }
    it { expect(calculator.calculate).to eql(result) }
  end

  context 'no elapsed time' do
    let(:start_date) { date_class.parse('07/11/1972') }
    let(:end_date) { date_class.parse('08/11/1972') }
    let(:result) { 0 }
    it { expect(calculator.calculate).to eql(result) }
  end

  context 'years' do
    let(:start_date) { date_class.parse('03/01/1989') }
    let(:end_date) { date_class.parse('03/08/1983') }
    let(:result) { 1979 }
    it { expect(calculator.calculate).to eql(result) }
  end

  context 'flips dates' do
    let(:start_date) { date_class.parse('04/07/1984') }
    let(:end_date) { date_class.parse('25/12/1984') }
    let(:result) { 173 }
    it { expect(calculator.calculate).to eql(result) }
  end

  context 'raise invalid input for ' do
    let(:start_date) { date_class.parse('04/07/3024') }
    let(:end_date) { date_class.parse('25/12/1984') }
    it do
      expect do
        DateCalculator::Calculator.new(start_date, end_date)
      end.to raise_error(DateCalculator::InvalidInput)
    end
  end
end
