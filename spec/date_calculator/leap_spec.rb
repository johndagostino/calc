require 'spec_helper'

class FancyDate
  attr_accessor :year
  def initialize(year)
    @year = year
  end

  include DateCalculator::Leap
end

describe DateCalculator::Leap do
  subject { FancyDate.new(year) }

  context 'leap' do
    let(:year) { 2016 }
    it { expect(subject.leap?).to eql(true) }
  end

  context 'mod 100' do
    let(:year) { 2100 }
    it { expect(subject.leap?).to eql(false) }
  end

  context 'not leap' do
    let(:year) { 2011 }
    it { expect(subject.leap?).to eql(false) }
  end
end
